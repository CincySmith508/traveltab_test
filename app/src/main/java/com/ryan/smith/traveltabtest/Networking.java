package com.ryan.smith.traveltabtest;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiConfiguration;
import android.net.wifi.WifiManager;
import android.util.Log;
import android.widget.Toast;


/**
 * Created by Smith on 2/11/18.
 */

public class Networking {

    private static final String TAG = "Networking.TAG";

    public static Boolean ConnectWithCredentials(Context mContext, String ssid, String passkey){

        Boolean needToConnect = needToConnectWifi(mContext);

        // First check if we need to connect to wifi
        if (!needToConnect){

            // Perform Connection
            return performConnection(mContext, ssid, passkey, needToConnect);

        } else {// Need To connect wifi

            // Connect to wifi
            enableWifi(mContext);

            // Perform Connection
            return performConnection(mContext, ssid, passkey, needToConnect);

        }// END if !needToConnectWifi

    }// END ConnectWithCredentials

    private static Boolean needToConnectWifi(Context mContext) {

        ConnectivityManager mgr = (ConnectivityManager) mContext.getSystemService(Context.CONNECTIVITY_SERVICE);

        if (mgr != null) {// Not null

            NetworkInfo networkInfo = mgr.getNetworkInfo(ConnectivityManager.TYPE_WIFI);

            if (networkInfo == null){

                Log.i(TAG, "needToConnectWifi: networkInfo is null");

                return true;

            } else {// Not null

                if (networkInfo.isConnected()) {// Connected to Wifi

                    Log.i(TAG, "needToConnectWifi: is Connected to wifi");
                    Log.i(TAG, "needToConnectWifi: networkInfo.getExtraInfo(): " + networkInfo.getExtraInfo());
                    return false;

                } else {// Not connected to wifi

                    Log.i(TAG, "needToConnectWifi: Not Connected to Wifi");
                    return true;

                }// END if networkInfo.isConnected

            }//END if networkInfo == null

        } else { // mgr is null

            Log.i(TAG, "needToConnectWifi: ConnectivityManager is null");

            return true;

        }//END  if mgr != null

    }//END needToConnectWifi

    private static void enableWifi(Context mContext){

        // Get wifi manager, use application context to avoid memory leaks on < N
        WifiManager wifi = (WifiManager) mContext.getApplicationContext().getSystemService(Context.WIFI_SERVICE);

        if (wifi != null){// Not null

            // Enable the wifi
            wifi.setWifiEnabled(true);

            Log.i(TAG, "enableWifi: enabling wifi");

        } else {// Manager is null

            Toast.makeText(mContext, "WifiManager is null", Toast.LENGTH_SHORT).show();

        }// END if wifi != null

    }// END enableWifi

    private static Boolean performConnection(Context mContext, String ssid, String passkey, Boolean hasWifiConnection){

        Log.i(TAG, "performConnection: ");

        // Get wifi manager, use application context to avoid memory leaks on < N
        WifiManager wifi = (WifiManager) mContext.getApplicationContext().getSystemService(Context.WIFI_SERVICE);

        if (wifi != null){// Not null

            Log.i(TAG, "performConnection:  creating configuration");

            // Create a wifi configuration
            WifiConfiguration wifiConfiguration = new WifiConfiguration();

            // Set the config's ssid and passkey values
            wifiConfiguration.SSID = String.format("\"%s\"", ssid);
            wifiConfiguration.preSharedKey = String.format("\"%s\"", passkey);

            // Check if hasWifiConnection is true then disconnect if it is.
            if (hasWifiConnection){

                wifi.disconnect();

            }// END if hasWifiConnection

            // Enable the network
            wifi.enableNetwork(wifi.addNetwork(wifiConfiguration), true);

            // Reconnect it.
            wifi.reconnect();

            return true;

        } else {// Manager is null

            Toast.makeText(mContext, "WifiManager is null", Toast.LENGTH_SHORT).show();

            return false;

        }// END if wifi != null

    }// END performConnection

}
