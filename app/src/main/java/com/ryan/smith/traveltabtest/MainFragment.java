package com.ryan.smith.traveltabtest;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.ryan.smith.traveltabtest.Data.CredentialsData;

import javax.inject.Inject;

/**
 * Created by Smith on 2/10/18.
 */

public class MainFragment extends Fragment {

    public static final String TAG = "MainFragment.TAG";

    @Inject
    CredentialsData credentialsData;

    public static MainFragment newInstance() {

        return new MainFragment();

    }// END newInstance

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        // Setup Inject
        TravelTabTestApplication.get(context).getCredentialsDataComponent().inject(this);

    }// END onAttach

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        // Setup Inject
        TravelTabTestApplication.get(activity).getCredentialsDataComponent().inject(this);

    }// END onAttach

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View fragView = inflater.inflate(R.layout.fragment_main, container, false);

        Button wifiButton = fragView.findViewById(R.id.main_wifi_button);
        wifiButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {

                // Connect with action
                TravelTabTestApplication.get(getActivity()).getWifiConnectActionCreator().activateWifi(getActivity(), credentialsData.getSsid(), credentialsData.getPasskey());

                Toast.makeText(getActivity(), "Attmepting to connect", Toast.LENGTH_SHORT).show();

            }// END onClick

        });// END OnClickListener

        return fragView;

    }// END onCreateView

}
