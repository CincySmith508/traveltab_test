package com.ryan.smith.traveltabtest;

import com.ryan.smith.traveltabtest.Data.CredentialsData;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Smith on 2/11/18.
 */

@Module
public class CredentialsDataModule {

    private static final String ttSSID = "TravelTab";
    private static final String ttPasskey = "123456789";

    public CredentialsDataModule() {
    }

    @Provides
    @Singleton
    CredentialsData providesCredentialsData() {

        return new CredentialsData(ttSSID, ttPasskey);

    }//END providesCredentialsData

}
