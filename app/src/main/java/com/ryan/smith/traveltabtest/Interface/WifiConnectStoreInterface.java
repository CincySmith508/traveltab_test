package com.ryan.smith.traveltabtest.Interface;

import com.ryan.smith.traveltabtest.Data.CredentialsData;

/**
 * Created by Smith on 2/10/18.
 */

public interface WifiConnectStoreInterface {

    CredentialsData getCredentials();

}// END Interface
