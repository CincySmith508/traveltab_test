package com.ryan.smith.traveltabtest.Data;

/**
 * Created by Smith on 2/10/18.
 */

public class CredentialsData  {

    private String ssid;
    private String passkey;

    public CredentialsData() {


    }// END constructor

    public CredentialsData(String ssid, String passkey) {
        this.ssid = ssid;
        this.passkey = passkey;
    }

    public String getSsid() {
        return ssid;
    }

    public String getPasskey() {
        return passkey;
    }
}
