package com.ryan.smith.traveltabtest;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

import com.hardsoftstudio.rxflux.action.RxError;
import com.hardsoftstudio.rxflux.dispatcher.RxViewDispatch;
import com.hardsoftstudio.rxflux.store.RxStore;
import com.hardsoftstudio.rxflux.store.RxStoreChange;
import com.ryan.smith.traveltabtest.Interface.Actions;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity implements RxViewDispatch {

    private static final String TAG = "MainActivity.TAG";

    private WifiConnectStore wifiConnectStore;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Launch the MainFragment
        MainFragment frag = MainFragment.newInstance();
        getSupportFragmentManager().beginTransaction().replace(R.id.activity_main, frag, MainFragment.TAG).commit();

    }// END onCreate

    @Override
    public void onRxStoreChanged(@NonNull RxStoreChange change) {

        switch (change.getStoreId()){

            case WifiConnectStore.StoreID:

                switch (change.getRxAction().getType()){

                    case Actions.ACTION_CONNECT_WIFI:

                        Log.i(TAG, "onRxStoreChanged: Action Activate Wifi");

                    default:
                        break;

                }// END switch change.getRxAction().getType()

                break;

            default:
                break;

        }// END switch(change.getStoreId())

    }// END onRxStoreChanged

    @Override
    public void onRxError(@NonNull RxError error) {

        Log.i(TAG, "onRxError: error: "  + error.getType());

    }// END onRxError

    @Override
    public void onRxViewRegistered() {

    }// END onRxViewRegistered

    @Override
    public void onRxViewUnRegistered() {

    }// END onRxViewUnRegistered

    @Nullable
    @Override
    public List<RxStore> getRxStoreListToRegister() {

        // Create empty ArrayList
        List<RxStore> storeList = new ArrayList<>();

        // Set credentialsStore
        wifiConnectStore = WifiConnectStore.get(TravelTabTestApplication.get(this).getRxFlux().getDispatcher());

        // Add to list
        storeList.add(wifiConnectStore);

        return storeList;

    }// END getRxStoreListToRegister

    @Nullable
    @Override
    public List<RxStore> getRxStoreListToUnRegister() {

        return null;

    }// END getRxStoreListToUnRegister

}
