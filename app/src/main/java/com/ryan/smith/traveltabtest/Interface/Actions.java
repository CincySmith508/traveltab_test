package com.ryan.smith.traveltabtest.Interface;

import android.content.Context;

/**
 * Created by Smith on 2/10/18.
 */

public interface Actions {

    String ACTION_CONNECT_WIFI = "action_connect_wifi";

    void activateWifi(Context mContext, String ssid, String passkey);

}// END Interface
