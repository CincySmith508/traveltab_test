package com.ryan.smith.traveltabtest.Interface;

import com.ryan.smith.traveltabtest.CredentialsDataModule;
import com.ryan.smith.traveltabtest.MainFragment;

import javax.inject.Singleton;

import dagger.Component;

/**
 * Created by Smith on 2/11/18.
 */

@Singleton
@Component(modules = CredentialsDataModule.class)
public interface CredentialsDataComponent {

    void inject(MainFragment fragment);

}// END Interface
