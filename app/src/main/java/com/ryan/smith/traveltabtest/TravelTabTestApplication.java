package com.ryan.smith.traveltabtest;

import android.app.Application;
import android.content.Context;

import com.hardsoftstudio.rxflux.RxFlux;
import com.ryan.smith.traveltabtest.Interface.CredentialsDataComponent;
import com.ryan.smith.traveltabtest.Interface.DaggerCredentialsDataComponent;

/**
 * Created by Smith on 2/10/18.
 */

public class TravelTabTestApplication extends Application {

    private static final String TAG = "TravTabTestApp.TAG";

    private RxFlux rxFlux;

    private WifiConnectActionCreator wifiConnectActionCreator;

    private CredentialsDataComponent credentialsDataComponent;

    public static TravelTabTestApplication get(Context context) {

        return ((TravelTabTestApplication) context.getApplicationContext());

    }// END get

    @Override
    public void onCreate() {
        super.onCreate();

        // Init rxFlux
        rxFlux = RxFlux.init(this);

        // Init the credentials action creator using the rx flux properties
        wifiConnectActionCreator = new WifiConnectActionCreator(rxFlux.getDispatcher(), rxFlux.getSubscriptionManager());

        // Get the credentialsDataComponent
        credentialsDataComponent = DaggerCredentialsDataComponent.builder().build();

    }// END onCreate

    public RxFlux getRxFlux() {

        return rxFlux;

    }// END getRxFlux

    public WifiConnectActionCreator getWifiConnectActionCreator() {

        return wifiConnectActionCreator;

    }// END getWifiConnectActionCreator

    public CredentialsDataComponent getCredentialsDataComponent() {

        return credentialsDataComponent;

    }// END getCredentialsDataComponent

    @Override
    public Context getApplicationContext() {

        return super.getApplicationContext();

    }// END getApplicationContext

}
