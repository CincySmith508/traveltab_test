package com.ryan.smith.traveltabtest;

import android.content.Context;
import android.util.Log;

import com.hardsoftstudio.rxflux.action.RxAction;
import com.hardsoftstudio.rxflux.action.RxActionCreator;
import com.hardsoftstudio.rxflux.dispatcher.Dispatcher;
import com.hardsoftstudio.rxflux.util.SubscriptionManager;
import com.ryan.smith.traveltabtest.Interface.Actions;
import com.ryan.smith.traveltabtest.Interface.Keys;

import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by Smith on 2/10/18.
 */

public class WifiConnectActionCreator extends RxActionCreator implements Actions {

    private static final String TAG = "WifiConActCreator.TAG";

    public WifiConnectActionCreator(Dispatcher dispatcher, SubscriptionManager manager) {

        super(dispatcher, manager);

    }// END constructor

    @Override
    public void activateWifi(final Context mContext, final String ssid, final String passkey) {

        final RxAction wifiAction = newRxAction(ACTION_CONNECT_WIFI, Keys.SSID, ssid, Keys.Passkey, passkey);

        // If action is already happening just return.
        if (hasRxAction(wifiAction)) return;

        addRxAction(wifiAction, Observable.create(new Observable.OnSubscribe() {
            @Override
            public void call(Object o) {

                Subscriber<Boolean> subscriber = ((Subscriber<Boolean>)o);
                subscriber.onNext(Networking.ConnectWithCredentials(mContext, ssid, passkey));
                subscriber.onCompleted();

            }
        }).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new Subscriber() {
            @Override
            public void onCompleted() {

                Log.i(TAG, "onCompleted: ");
                postRxAction(wifiAction);

            }// END onComplete

            @Override
            public void onError(Throwable e) {

            }// END onError

            @Override
            public void onNext(Object o) {

                Log.i(TAG, "onNext: bool : " + ((Boolean)o));
                
            }// END onNext

        }));// END subscriber

    }// END activateWifi

}
