package com.ryan.smith.traveltabtest;

import android.util.Log;

import com.hardsoftstudio.rxflux.action.RxAction;
import com.hardsoftstudio.rxflux.dispatcher.Dispatcher;
import com.hardsoftstudio.rxflux.store.RxStore;
import com.hardsoftstudio.rxflux.store.RxStoreChange;
import com.ryan.smith.traveltabtest.Data.CredentialsData;
import com.ryan.smith.traveltabtest.Interface.Actions;
import com.ryan.smith.traveltabtest.Interface.Keys;
import com.ryan.smith.traveltabtest.Interface.WifiConnectStoreInterface;

/**
 * Created by Smith on 2/10/18.
 */

public class WifiConnectStore extends RxStore implements WifiConnectStoreInterface {

    private static final String TAG = "WifiConnectStore.TAG";

    public static final String StoreID = "com.ryan.smith.traveltabtest.WifiConnectStoreID";

    private String ssid;
    private String passkey;

    private static WifiConnectStore wifiConnectStoreInstance;

    public WifiConnectStore(Dispatcher dispatcher) {

        super(dispatcher);

    }// END constructor

    // Used to allow for the interface to be accessed by the view.
    public static synchronized WifiConnectStore get(Dispatcher dispatcher) {

        if (wifiConnectStoreInstance == null){

            // Set new instance
            wifiConnectStoreInstance = new WifiConnectStore(dispatcher);

            // Return it.
            return wifiConnectStoreInstance;

        } else {// Not null

            return wifiConnectStoreInstance;

        }// END if wifiConnectStoreInstance == null

    }// END get instance

    @Override
    public void onRxAction(RxAction action) {

        switch (action.getType()){

            case Actions.ACTION_CONNECT_WIFI:

                Log.i(TAG, "onRxAction: Action Activate Wifi ");

                // Set the values for the store
                this.ssid = action.get(Keys.SSID);
                this.passkey = action.get(Keys.Passkey);

                // Post the change
                postChange(new RxStoreChange(StoreID, action));

            default:
                break;

        }// END switch(action.getType())

    }// END onRxAction

    @Override
    public CredentialsData getCredentials() {

        return new CredentialsData(ssid, passkey);

    }// END getCredentials

}
